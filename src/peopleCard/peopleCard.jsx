// libraries
import React, { useState } from "react"

// imports
import { dataToShow } from "../configs"

export const PeopleCard = props => {
    const { person } = props

    const [overData, setOverData] = useState("")
    return (
        <div className="person">
            <div className="person-img">
                <div className="img-container">
                    <img src={person.picture.large} alt="" />
                </div>
            </div>
            <div className="over-data">
                {!!overData && (
                    <>
                        <div className="title">{`My ${overData.title} is`}</div>
                        <div className="data">{overData.data}</div>
                    </>
                )}
            </div>
            <div className="person-data">
                {dataToShow.map(item => {
                    let data = person[item.data]

                    return (
                        <div
                            key={item.tag}
                            className="data-list"
                            onMouseEnter={() =>
                                setOverData({
                                    title: item.tag,
                                    data: !!item.render
                                        ? item.render(data)
                                        : data,
                                })
                            }
                        >
                            <item.icon className="icons" />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
