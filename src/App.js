// libraries
import React, { useEffect, useState } from "react"
import Loader from "react-loader-spinner"

// components
import { PeopleCard } from "./peopleCard/peopleCard"

// imports
import { ENDPOINT } from "./constants.js"

const App = () => {
    const [isLoading, setIsLoading] = useState(true)
    const [peopleData, setPeopleData] = useState([])

    useEffect(() => {
        async function fetchUser() {
            const data = await (await fetch(ENDPOINT)).json()
            if (data) {
                setPeopleData(data.results)
            }
            setIsLoading(false)
        }

        fetchUser()
    }, [])

    return (
        <div className="app-container">
            {isLoading ? (
                <Loader type="Puff" color="#ccc" />
            ) : (
                peopleData.map(person => (
                    <PeopleCard person={person} key={person.login.username} />
                ))
            )}
        </div>
    )
}

export default App
