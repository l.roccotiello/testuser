// libraries
import { format } from "date-fns"

// components
import { MailIcon } from "./icons/mailIcon"
import { MapIcon } from "./icons/mapIcon"
import { LockIcon } from "./icons/lockIcon"
import { UserIcon } from "./icons/userIcon"
import { PhoneIcon } from "./icons/phoneIcon"
import { CalendarIcon } from "./icons/calendarIcon"

export const dataToShow = [
    {
        tag: "name",
        icon: UserIcon,
        data: "name",
        render: ({ first, last }) => `${first} ${last}`,
        // subData: ["first", "last"],
    },
    {
        tag: "email address",
        icon: MailIcon,
        data: "email",
    },
    {
        tag: "birthdate",
        icon: CalendarIcon,
        data: "dob",
        render: ({ date }) => format(new Date(date), "dd/MM/yyyy"),
        // subData: ["date"],
    },
    {
        tag: "address",
        icon: MapIcon,
        data: "location",
        render: ({ city, street: { number, name } }) =>
            `${name} ${number}, ${city}`,
        // subData: ["city"],
    },
    {
        tag: "phone number",
        icon: PhoneIcon,
        data: "phone",
    },
    {
        tag: "password",
        icon: LockIcon,
        data: "login",
        render: ({ password }) => password,
        // subData: ["password"],
    },
]
